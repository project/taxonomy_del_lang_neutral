CONTENTS OF THIS FILE
=====================

 * Introduction
 * Installation
 * Usage
 * Author

INTRODUCTION
============
In order to hide default Language Neutral option from node add/edit form, i18n
contains <em>i18n_node</em> module that provide an admin interface to manage
this option.  Similarly for taxonomy terms I was looking for some module but 
I did not get. Hence, just created this module to delete/unset Language Neutral
option from Language dropdown for taxonomy only.

INSTALLATION
============

To install this module, follow given steps:

  * Place this module directory in your modules folder (this will usually be
    "sites/all/modules/").
  * Enable the module within your Drupal site at Administer -> Site Building ->
    Modules (admin/build/modules).

USAGE
=====
  * Go to taxonomy page and edit any vocabulary.
  * If you choose "Translate Different Terms will be allowed for each language
    and then can be translated", then "Delete language neutral from terms"
    option automatically appears.
  * If you check this option then Language Neutral option will be unset while
    add/edit any term from Language dropdown. 

AUTHOR
======
Pushpinder Rana: https://www.drupal.org/u/er.pushpinderrana
